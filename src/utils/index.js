/**
 * Created by PanJiaChen on 16/11/18.
 */

import { connrequest } from '@/api/firstPage'
/**
 * 获取设备树结构数据
 * @returns {Promise<Array>} 返回树结构的 Promise
 */
export async function getDeviceTreeData(childKey = 'Children') {
  let deviceObjList = []
  try {
    // 获取部门数据
    const deptResponse = await connrequest('GetDepartmentDeviceSumTree')
    const departments = deptResponse.Data.Department
    // 获取所有设备数据
    const data = {
      Paging: {
        QueryPageNo: 1,
        QueryPageLength: 100000
      }
    }
    const deviceResponse = await await connrequest('GetDevice', data) // 你需要确认是否有这个接口
    const devices = deviceResponse.Data

    // 生成树结构
    deviceObjList = initDeviceTree(departments, devices, childKey)
    return deviceObjList
  } catch (error) {
    return []
  }
}

// 获取资产树
export async function GetDeviceTree(childKey = 'Children') {
  let deviceObjList = []
  try {
    // 获取部门数据
    const deptResponse = await connrequest('GetDepartmentDeviceSumTree')
    const departments = deptResponse.Data.Department
    const rootName = deptResponse.Data.RootName
    // 获取所有设备数据
    const data = {
      Paging: {
        QueryPageNo: 1,
        QueryPageLength: 100000
      }
    }
    const deviceResponse = await await connrequest('GetDevice', data) // 你需要确认是否有这个接口
    const devices = deviceResponse.Data

    // 生成树结构
    deviceObjList = initDeviceTree(departments, devices)
    return [{
      Name: rootName || '全部',
      ID: 0,
      ParentID: "",
      ParentName: '',
      [childKey]: deviceObjList,
    }]
  } catch (error) {
    return []
  }
}
// 将设备数据转换为 el-cascader 树结构的函数
export function initDeviceTree(departments, devices, childKey = 'Children') {
  let deviceObjList = []
  // 创建部门映射表
  const departmentMap = new Map();
  const departmentClones = departments.map(dept => ({
    ...dept,
    [childKey]: []
  }));
  // 建立父子关系
  departmentClones.forEach(dept => {
    departmentMap.set(dept.ID, dept);
    if (dept.ParentID === 0) {
      deviceObjList.push(dept);
    } else {
      const parent = departmentMap.get(dept.ParentID);
      parent && parent[childKey].push(dept);
    }
  });
  // 将设备挂载到对应部门
  devices.forEach(device => {
    const dept = departmentMap.get(device.DepartmentID);
    if (dept) {
      dept[childKey].push({
        ...device,
        ID: device.DeviceUUID,
        Name: device.DeviceName || device.DeviceCode || '未命名设备',
        [childKey]: null,
        leaf: false,
        isDevice: true
      });
    }
  });
  return deviceObjList
}


// 设置部门树
export function transformToCascaderTree(data) {
  // 创建一个 Map 来存储所有节点，便于查找
  const nodeMap = new Map();

  // 先将所有数据放入 Map，并初始化基本结构
  data.forEach(item => {
    nodeMap.set(item.ID, {
      ID: item.ID,
      Name: item.Name,
      Children: []  // 初始化空子节点数组
    });
  });

  // 创建结果数组
  const result = [];

  // 第二次遍历，建立父子关系
  data.forEach(item => {
    const currentNode = nodeMap.get(item.ID);

    if (item.ParentID === 0) {
      // 如果 ParentID 为 0，是根节点，直接加入结果数组
      result.push(currentNode);
    } else {
      // 如果有父节点，将当前节点加入父节点的 Children 数组
      const parentNode = nodeMap.get(item.ParentID);
      if (parentNode) {
        parentNode.Children.push(currentNode);
      }
    }
  });

  // 清理空的 Children 数组
  function cleanEmptyChildren(node) {
    if (node.Children.length === 0) {
      delete node.Children;
    } else {
      node.Children.forEach(cleanEmptyChildren);
    }
  }

  result.forEach(cleanEmptyChildren);

  return result;
}

/**
 * 根据 DeviceObjType 返回对应的格式化字符串
 * @param {Object} row - 数据行，包含 DeviceObjType 属性
 * @returns {string} 格式化后的字符串
 */
export function DeviceObjTypeFormatter(row) {
  const typeMap = {
    1: '全部',
    2: '指定部门',
    8: '指定角色',
    6: '指定资产'
  };

  return typeMap[row.DeviceObjType] || '其他';
}

/**
 * 根据 DeviceObjType 和 DeviceObj 获取显示的字符串
 * @param {Object} [row={}] - 数据行，默认为空对象
 * @param {Array} [departments=[]] - 部门数据，默认为空数组
 * @param {Array} [assets=[]] - 资产数据，默认为空数组
 * @returns {string} 处理后的显示字符串
 */
export function getDeviceObjDisplay(row = {}, departments = [], assets = []) {
  const { DeviceObjType, DeviceObj = [] } = row;

  if (DeviceObjType === 1) return '全部';
  else if (DeviceObjType === 2) {
    const deptNames = DeviceObj.map(id => {
      const dept = departments.find(d => d.ID === id);
      return dept ? dept.DepartmentName : '未知部门';
    });
    return deptNames.join(', ');
  } else if (DeviceObjType === 8) {
    return DeviceObj.join(', ');
  } else if (DeviceObjType === 6) {
    const assetNames = DeviceObj.map(uuid => {
      const asset = assets.find(a => a.DeviceUUID === uuid);
      return asset ? asset.DeviceName : '未知资产';
    });
    return assetNames.join(', ');
  }

  return '未知类型';
}

export function validatePort(value) {
  let port = value.replace(/\D/g, ''); // 移除非数字字符
  if (port > 65535) port = 65535; // 限制最大值
  if (port < 0) port = 0; // 限制最小值
  return port
}
/**
 * 通用方法，将一个列表中的特定属性值映射到另一个列表的对应属性值
 * @param {Array} sourceList - 需要转换的列表
 * @param {Array} targetList - 包含目标映射属性的列表
 * @param {string} sourceKey - 源列表中用于查找的键名
 * @param {string} targetKey - 目标列表中用于查找的键名
 * @returns {Array} - 转换后的目标值数组
 */
export function convertListToNames(sourceList, targetList, sourceKey, targetKey) {
  return sourceList.map(sourceItem => {
    const targetItem = targetList.find(target => target[sourceKey] === sourceItem);
    return targetItem ? targetItem[targetKey] : null; // 如果找到对应的目标项，返回目标值，否则返回 null
  }).filter(name => name !== null); // 过滤掉没有找到目标值的项
}


// 获取子节点的资产
export async function lazyLoad(node) {
  const data = {
    Paging: {
      QueryPageNo: 1,
      QueryPageLength: 100000
    },
    Filter: {
      SchoolName: node.SchoolName
    }
  }
  const res = await connrequest('CenterGetDevice', data)
  let arr = []
  res.Data.map(item => {
    arr.push({ SubClass: item.DeviceName, value: item.DeviceName, level: 5, children: undefined, leaf: true })
  })
  return arr
}
// 生成UUID
export function getuuid() {
  const chars = '0123456789';//abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ
  let result = '';
  for (let i = 12; i > 0; --i) {
    result += chars[Math.floor(Math.random() * chars.length)];
  }
  return result;
}

// 字节转换为单位大小
export function bytesToSize(bytes) {
  if (bytes === 0) return '0 B';
  var k = 1024;
  var sizes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
  var i = Math.floor(Math.log(bytes) / Math.log(k));
  return (bytes / Math.pow(k, i)).toFixed(sizes[i] === 'B' ? 0 : 2) + ' ' + sizes[i];
}
// export function bytesToSize(bytes) {
//   if(bytes === 0) return '0 B';
//   var k = 1024;
//   var sizes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
//   var i = Math.floor(Math.log(bytes) / Math.log(k));
//   return (bytes / Math.pow(k, i)).toFixed(0) + ' ' + sizes[i];
// }

// 转树结构
export function toTree(data, rootName) {
  var map = {};  // 用来存储 ID 和对应节点的映射关系
  var roots = [];  // 存储根节点
  // 首先遍历一次数据，将每个节点存储到 map 中
  data.forEach(function (node) {
    map[node.ID] = { ...node, Children: [] };  // 将 current node 放进 map，并初始化 children
  });
  // 再次遍历数据，将每个节点链接到它的父节点
  data.forEach(function (node) {
    if (node.ParentID !== 0) {
      map[node.ParentID].Children.push(map[node.ID]);
    } else {
      roots.push(map[node.ID]);  // 如果是根节点，直接推进根节点数组
    }
  });
  // 返回树形结构
  return replaceEmptyChildrenWithUndefined([{
    Name: rootName || '全部',
    ID: 0,
    ParentID: "",
    ParentName: '',
    Children: roots
  }]);
}

export function replaceEmptyChildrenWithUndefined(data) {
  if (Array.isArray(data)) {
    return data.map(item => {
      if (item.Children && item.Children.length === 0) {
        return { ...item, Children: undefined };
      } else {
        return { ...item, Children: replaceEmptyChildrenWithUndefined(item.Children) };
      }
    });
  } else {
    return data;
  }
}
// 获取域名主分类
export function Maintype(mainId) {
  if (mainId * 1 === 0) {
    return `未知分类`
  } else {
    let arr = JSON.parse(window.sessionStorage.getItem('ParamDictURL'))
    let newarr = arr.filter(item => { return item.MainID === mainId * 1 })
    return `${newarr[0].MainClass}`
  }
}

// 获取域名子分类
export function ParamDictURLtype(classId) {
  if (classId * 1 === 0) {
    return `未知分类`
  } else {
    let arr = JSON.parse(window.sessionStorage.getItem('ParamDictURL'))
    let newarr = arr.filter(item => { return item.ClassID === classId * 1 })
    return `${newarr[0].MainClass} / ${newarr[0].SubClass}`
  }
}


// 扁平化数据转为树
export function treeing(arr, id, parentid) {
  let tree = []
  const map = {}
  for (let item of arr) {
    // 一个新的带children的结构
    let newItem = map[item.ID || id] = {
      ...item,
      children: []
    }
    if (map[item.ParentID || parentid]) { // 父节点已存进map则在父节点的children添加新元素
      let parent = map[item.ParentID || parentid]
      parent.children.push(newItem)
    } else { // 没有父节点，在根节点添加父节点
      tree.push(newItem)
    }
  }
  return tree
}

// 递归判断列表，把最后的children设为undefined
export function getTreeData(data) {
  for (var i = 0; i < data.length; i++) {
    if (data[i].children.length < 1) {
      // children若为空数组，则将children设为undefined
      data[i].children = undefined;
    } else {
      // children若不为空数组，则继续 递归调用 本方法
      getTreeData(data[i].children);
    }
  }
  return data;
}
export function buildDepartmentTree(data) {
  // 创建一个空对象存储每个部门的信息
  const departmentDict = {};
  for (const d of data) {
    departmentDict[d.ID] = {
      name: d.DepartmentName,
      children: []
    };
  }

  // 遍历数据，将子节点添加到父节点的 children 列表中
  for (const d of data) {
    const parentID = d.ParentID;
    if (parentID !== 0) {
      departmentDict[parentID].children.push(departmentDict[d.ID]);
    }
  }

  // 找到根节点并返回
  let root = null;
  for (const d of Object.values(departmentDict)) {
    if (d.name === '鑫塔科技') {
      root = d;
      break;
    }
  }

  // 统计每个部门的子节点数量
  function countChildren(node) {
    let count = node.children.length;
    for (const child of node.children) {
      count += countChildren(child);
    }
    return count;
  }
  countChildren(root);

  return root;
}

// 获取到所有父节点
export function recursiveGetNodePath(node, path) {
  if (node) {
    if (node.data.Name !== undefined) {
      path.unshift(node.data.Name);
    }
    if (node.parent !== null) {
      recursiveGetNodePath(node.parent, path);
    }
  }
  return path
}

// 获取到所有子节点
export function recursiveGetChildNodePath(node, path) {
  if (node) {
    if (node.data.Name !== undefined) {
      path.push(node.data.Name);
    }
    if (node.childNodes !== null) {
      recursiveGetChildNodePath(node.childNodes[0], path);
    }
  }
  return path
}


export function parseTime(time, cFormat) {
  if (arguments.length === 0 || !time) {
    return null
  }
  const format = cFormat || '{y}-{m}-{d} {h}:{i}:{s}'
  let date
  if (typeof time === 'object') {
    date = time
  } else {
    if ((typeof time === 'string')) {
      if ((/^[0-9]+$/.test(time))) {
        // support "1548221490638"
        time = parseInt(time)
      } else {
        // support safari
        // https://stackoverflow.com/questions/4310953/invalid-date-in-safari
        time = time.replace(new RegExp(/-/gm), '/')
      }
    }

    if ((typeof time === 'number') && (time.toString().length === 10)) {
      time = time * 1000
    }
    date = new Date(time)
  }
  const formatObj = {
    y: date.getFullYear(),
    m: date.getMonth() + 1,
    d: date.getDate(),
    h: date.getHours(),
    i: date.getMinutes(),
    s: date.getSeconds(),
    a: date.getDay()
  }
  const time_str = format.replace(/{([ymdhisa])+}/g, (result, key) => {
    const value = formatObj[key]
    // Note: getDay() returns 0 on Sunday
    if (key === 'a') { return ['日', '一', '二', '三', '四', '五', '六'][value] }
    return value.toString().padStart(2, '0')
  })
  return time_str
}

export function
  formatDuration(durationInSeconds) {
  const oneMinute = 60;
  const oneHour = oneMinute * 60;
  const oneDay = oneHour * 24;
  const days = Math.floor(durationInSeconds / oneDay);
  const hours = Math.floor((durationInSeconds % oneDay) / oneHour);
  const minutes = Math.floor((durationInSeconds % oneHour) / oneMinute);
  const seconds = Math.floor(durationInSeconds % oneMinute);
  let formattedDuration = '';
  if (days > 0) {
    formattedDuration += `${days}天${hours}小时`;
  } else if (hours > 0) {
    formattedDuration += `${hours}小时${minutes}分钟`;
  } else if (minutes > 0) {
    formattedDuration += `${minutes}分钟${seconds}秒`;
  } else {
    formattedDuration += `${seconds}秒`;
  }
  return formattedDuration;
}
export function formatDuration2(durationInSeconds) {
  const oneMinute = 60;
  const oneHour = oneMinute * 60;
  const oneDay = oneHour * 24;
  const days = Math.floor(durationInSeconds / oneDay);
  const hours = Math.floor((durationInSeconds % oneDay) / oneHour);
  const minutes = Math.floor((durationInSeconds % oneHour) / oneMinute);
  const seconds = Math.floor(durationInSeconds % oneMinute);
  const days2 = days * 24 + hours
  let formattedDuration = '';
  if (days > 0) {
    formattedDuration += `${days * 24 + hours}小时`;
  } else
    if (hours > 0) {
      formattedDuration += `${hours}小时`;
    } else if (minutes > 0) {
      formattedDuration += `${minutes}分钟`;
    } else {
      formattedDuration += `${seconds}秒`;
    }
  return formattedDuration;
}

export function formatBytes(bytes) {
  if (bytes < 1024) {
    return bytes + " 字节";
  } else if (bytes < 1048576) {
    return (bytes / 1024).toFixed(2) + " KB";
  } else if (bytes < 1073741824) {
    return (bytes / 1048576).toFixed(2) + " MB";
  } else {
    return (bytes / 1073741824).toFixed(2) + " GB";
  }
}
export function formatBytes2(bytes) {
  if (!bytes) {
    return 0
  } else if (bytes < 1000) {
    return bytes + " 字节";
  } else if (bytes < 1000000) {
    return Math.floor(bytes / 1000) + " K";
  } else if (bytes < 1000000000) {
    return Math.floor(bytes / 1000000) + " M";
  } else if (bytes < 1000000000000) {
    return Math.floor(bytes / 1000000000) + " G";
  } else {
    return Math.floor(bytes / 1000000000000) + " T";
  }
}

export function formatBytes_back(bytes) {
  if (bytes < 1024) {
    return bytes + " M";
  } else if (bytes < 1048576) {
    return (bytes / 1024).toFixed(2) + " G";
  } else if (bytes < 1073741824) {
    return (bytes / 1048576).toFixed(2) + " T";
  }
}

// 获取当日
export function getNowDay(time) {
  // 判断是否需要时间，undefined：不需要   time: 默认0点 || 自定义时间
  time = time ? (time == 'time' ? '00:00:00' : time) : ''
  const date = new Date()
  let y = date.getFullYear();
  let m = date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1;
  let d = date.getDate();
  return `${y}-${m}-${d} ${time}`
}
/**
 * @param {number} time
 * @param {string} option
 * @returns {string}
 */
export function formatTime(time, option) {
  if (('' + time).length === 10) {
    time = parseInt(time) * 1000
  } else {
    time = +time
  }
  const d = new Date(time)
  const now = Date.now()

  const diff = (now - d) / 1000

  if (diff < 10) {
    return '刚刚'
  } else if (diff < 60) {
    return Math.ceil(diff) + ' 秒前'
  } else if (diff < 3600) {
    return Math.ceil(diff / 60) + ' 分钟前'
  } else if (diff < 3600 * 24) {
    return Math.ceil(diff / 3600) + ' 小时前'
  } else if (diff < 3600 * 24 * 2) {
    return '1天前'
  }
  if (option) {
    return parseTime(time, option)
  } else {
    return (
      d.getMonth() +
      1 +
      '月' +
      d.getDate() +
      '日' +
      d.getHours() +
      '时' +
      d.getMinutes() +
      '分'
    )
  }
}
export function formatTime2(time, option) {
  if (('' + time).length === 10) {
    time = parseInt(time) * 1000
  } else {
    time = +time
  }
  const d = new Date(time)
  const now = Date.now()
  const diff = (now - d) / 1000
  if (time === 0) {
    return '初始化'
  }
  else if (diff < 10) {
    return '刚刚'
  } else if (diff < 60) {
    return Math.ceil(diff) + ' 秒前'
  } else if (diff < 3600) {
    const minutes = Math.floor(diff / 60);
    const seconds = Math.floor(diff % 60);
    return `${minutes} 分钟 ${seconds} 秒前`;
  } else if (diff < 3600 * 24) {
    const hours = Math.floor(diff / 3600);
    const minutes = Math.floor((diff % 3600) / 60);
    return `${hours} 小时 ${minutes} 分钟前`;
  } else {
    const days = Math.floor(diff / (3600 * 24));
    const hours = Math.floor((diff % (3600 * 24)) / 3600);
    return `${days} 天 ${hours} 小时前`;
  }
}

export function formatTime3(time) {
  if (('' + time).length === 10) {
    time = parseInt(time) * 1000;
  } else {
    time = +time;
  }
  const d = new Date(time);
  const now = Date.now();
  const diff = (now - d) / 1000;

  const yearDiff = Math.floor(diff / (3600 * 24 * 365));
  const monthDiff = Math.floor(diff / (3600 * 24 * 30)) % 12;
  const dayDiff = Math.floor(diff / (3600 * 24)) % 30;

  if (yearDiff > 0) {
    return `${yearDiff} 年 ${monthDiff} 月 ${dayDiff} 天`;
  } else if (monthDiff > 0) {
    return `${monthDiff} 月 ${dayDiff} 天`;
  } else {
    return `${dayDiff} 天`;
  }
}
/**
 * @param {string} url
 * @returns {Object}
 */
export function getQueryObject(url) {
  url = url == null ? window.location.href : url
  const search = url.substring(url.lastIndexOf('?') + 1)
  const obj = {}
  const reg = /([^?&=]+)=([^?&=]*)/g
  search.replace(reg, (rs, $1, $2) => {
    const name = decodeURIComponent($1)
    let val = decodeURIComponent($2)
    val = String(val)
    obj[name] = val
    return rs
  })
  return obj
}

/**
 * @param {string} input value
 * @returns {number} output value
 */
export function byteLength(str) {
  // returns the byte length of an utf8 string
  let s = str.length
  for (var i = str.length - 1; i >= 0; i--) {
    const code = str.charCodeAt(i)
    if (code > 0x7f && code <= 0x7ff) s++
    else if (code > 0x7ff && code <= 0xffff) s += 2
    if (code >= 0xDC00 && code <= 0xDFFF) i--
  }
  return s
}

/**
 * @param {Array} actual
 * @returns {Array}
 */
export function cleanArray(actual) {
  const newArray = []
  for (let i = 0; i < actual.length; i++) {
    if (actual[i]) {
      newArray.push(actual[i])
    }
  }
  return newArray
}

/**
 * @param {Object} json
 * @returns {Array}
 */
export function param(json) {
  if (!json) return ''
  return cleanArray(
    Object.keys(json).map(key => {
      if (json[key] === undefined) return ''
      return encodeURIComponent(key) + '=' + encodeURIComponent(json[key])
    })
  ).join('&')
}

/**
 * @param {string} url
 * @returns {Object}
 */
export function param2Obj(url) {
  const search = decodeURIComponent(url.split('?')[1]).replace(/\+/g, ' ')
  if (!search) {
    return {}
  }
  const obj = {}
  const searchArr = search.split('&')
  searchArr.forEach(v => {
    const index = v.indexOf('=')
    if (index !== -1) {
      const name = v.substring(0, index)
      const val = v.substring(index + 1, v.length)
      obj[name] = val
    }
  })
  return obj
}

/**
 * @param {string} val
 * @returns {string}
 */
export function html2Text(val) {
  const div = document.createElement('div')
  div.innerHTML = val
  return div.textContent || div.innerText
}

/**
 * Merges two objects, giving the last one precedence
 * @param {Object} target
 * @param {(Object|Array)} source
 * @returns {Object}
 */
export function objectMerge(target, source) {
  if (typeof target !== 'object') {
    target = {}
  }
  if (Array.isArray(source)) {
    return source.slice()
  }
  Object.keys(source).forEach(property => {
    const sourceProperty = source[property]
    if (typeof sourceProperty === 'object') {
      target[property] = objectMerge(target[property], sourceProperty)
    } else {
      target[property] = sourceProperty
    }
  })
  return target
}

/**
 * @param {HTMLElement} element
 * @param {string} className
 */
export function toggleClass(element, className) {
  if (!element || !className) {
    return
  }
  let classString = element.className
  const nameIndex = classString.indexOf(className)
  if (nameIndex === -1) {
    classString += '' + className
  } else {
    classString =
      classString.substr(0, nameIndex) +
      classString.substr(nameIndex + className.length)
  }
  element.className = classString
}

/**
 * @param {string} type
 * @returns {Date}
 */
export function getTime(type) {
  if (type === 'start') {
    return new Date().getTime() - 3600 * 1000 * 24 * 90
  } else {
    return new Date(new Date().toDateString())
  }
}

/**
 * @param {Function} func
 * @param {number} wait
 * @param {boolean} immediate
 * @return {*}
 */
export function debounce(func, wait, immediate) {
  let timeout, args, context, timestamp, result

  const later = function () {
    // 据上一次触发时间间隔
    const last = +new Date() - timestamp

    // 上次被包装函数被调用时间间隔 last 小于设定时间间隔 wait
    if (last < wait && last > 0) {
      timeout = setTimeout(later, wait - last)
    } else {
      timeout = null
      // 如果设定为immediate===true，因为开始边界已经调用过了此处无需调用
      if (!immediate) {
        result = func.apply(context, args)
        if (!timeout) context = args = null
      }
    }
  }

  return function (...args) {
    context = this
    timestamp = +new Date()
    const callNow = immediate && !timeout
    // 如果延时不存在，重新设定延时
    if (!timeout) timeout = setTimeout(later, wait)
    if (callNow) {
      result = func.apply(context, args)
      context = args = null
    }

    return result
  }
}

/**
 * This is just a simple version of deep copy
 * Has a lot of edge cases bug
 * If you want to use a perfect deep copy, use lodash's _.cloneDeep
 * @param {Object} source
 * @returns {Object}
 */
export function deepClone(source) {
  if (!source && typeof source !== 'object') {
    throw new Error('error arguments', 'deepClone')
  }
  const targetObj = source.constructor === Array ? [] : {}
  Object.keys(source).forEach(keys => {
    if (source[keys] && typeof source[keys] === 'object') {
      targetObj[keys] = deepClone(source[keys])
    } else {
      targetObj[keys] = source[keys]
    }
  })
  return targetObj
}

/**
 * @param {Array} arr
 * @returns {Array}
 */
export function uniqueArr(arr) {
  return Array.from(new Set(arr))
}

/**
 * @returns {string}
 */
export function createUniqueString() {
  const timestamp = +new Date() + ''
  const randomNum = parseInt((1 + Math.random()) * 65536) + ''
  return (+(randomNum + timestamp)).toString(32)
}

/**
 * Check if an element has a class
 * @param {HTMLElement} elm
 * @param {string} cls
 * @returns {boolean}
 */
export function hasClass(ele, cls) {
  return !!ele.className.match(new RegExp('(\\s|^)' + cls + '(\\s|$)'))
}

/**
 * Add class to element
 * @param {HTMLElement} elm
 * @param {string} cls
 */
export function addClass(ele, cls) {
  if (!hasClass(ele, cls)) ele.className += ' ' + cls
}

/**
 * Remove class from element
 * @param {HTMLElement} elm
 * @param {string} cls
 */
export function removeClass(ele, cls) {
  if (hasClass(ele, cls)) {
    const reg = new RegExp('(\\s|^)' + cls + '(\\s|$)')
    ele.className = ele.className.replace(reg, ' ')
  }
}


export function validateInput(input) {
  const entries = input.split('\n').map(e => e.trim()).filter(e => e);
  return entries.every(validateEntry);
}

function validateEntry(entry) {
  // 检查端口或端口范围
  if (/^\d+(-\d+)?$/.test(entry)) {
    return validatePort2(entry);
  }

  // 检查IPv6地址（带方括号）及端口
  const ipv6WithPort = /^\[([\da-fA-F:]+)\](?::(\d+(-\d+)?))?$/;
  const ipv6Match = entry.match(ipv6WithPort);
  if (ipv6Match) {
    return validateIPv6(ipv6Match[1]) && (!ipv6Match[2] || validatePort2(ipv6Match[2]));
  }

  // 分离地址和端口部分（非IPv6）
  const [address, port] = entry.split(/:(?=\d+(-\d+)?$)/);
  const hasPort = port !== undefined;

  // 检查CIDR（不能带端口）
  if (validateCIDR(address)) {
    return !hasPort;
  }

  // 检查IPv4地址或范围
  if (validateIPv4(address) || validateIPv4Range(address)) {
    return !hasPort || validatePort2(port);
  }

  return false;
}

function validatePort2(port) {
  if (port.includes('-')) {
    const [start, end] = port.split('-').map(Number);
    return start >= 0 && end <= 65535 && start <= end;
  }
  const num = parseInt(port, 10);
  return num >= 0 && num <= 65535;
}

function validateIPv4(ip) {
  return /^((25[0-5]|2[0-4]\d|[01]?\d\d?)\.){3}(25[0-5]|2[0-4]\d|[01]?\d\d?)$/.test(ip);
}

function validateIPv4Range(range) {
  const [start, end] = range.split('-');
  return validateIPv4(start) && validateIPv4(end) && isIPv4LessOrEqual(start, end);
}

function isIPv4LessOrEqual(a, b) {
  const aParts = a.split('.').map(Number);
  const bParts = b.split('.').map(Number);
  for (let i = 0; i < 4; i++) {
    if (aParts[i] > bParts[i]) return false;
    if (aParts[i] < bParts[i]) return true;
  }
  return true;
}

function validateCIDR(cidr) {
  const [ip, prefix] = cidr.split('/');
  const prefixNum = parseInt(prefix, 10);
  if (validateIPv4(ip)) {
    return prefixNum >= 0 && prefixNum <= 32;
  }
  if (validateIPv6(ip)) {
    return prefixNum >= 0 && prefixNum <= 128;
  }
  return false;
}

function validateIPv6(ip) {
  // 简化的IPv6验证（允许压缩格式）
  const pattern = /^(([0-9a-fA-F]{1,4}:){7}[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){0,7}::([0-9a-fA-F]{1,4}:){0,7}[0-9a-fA-F]{1,4}|::)$/;
  return pattern.test(ip);
}
