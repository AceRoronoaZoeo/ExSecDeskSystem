import defaultSettings from '@/settings'
import i18n from '@/lang'
const Glod = require('../../public/config')

const title = Glod.pageTitle || '卓越桌面管理中心'

export default function getPageTitle(key) {
  const hasKey = i18n.te(`route.${key}`)
  if (hasKey) {
    const pageName = i18n.t(`route.${key}`)
    return `${pageName} - ${title}`
  }
  return `${title}`
}
