import request from '@/utils/request'
import { getToken } from '@/utils/auth' // get token from cookie
const requesturl = '/cfunc'
const customurl = '/emsg/'
const downfileurl = '/download'
const upload = '/upload'
import axios from 'axios';

// 通用方法
export function connrequest2(CmdCode, data) {
  return new Promise((resolve, reject) => {
    axios.post(requesturl, {
      ...data,
      Header: {
        CmdCode: CmdCode,
      },
      Token: getToken()
    }, {
      timeout: 0 // 设置超时时间为0，表示无限等待
    })
    .then(response => {
      resolve(response.data);
    })
    .catch(error => {
      reject(error);
    });
  });
}

// 通用方法
export function connrequest(CmdCode, data) {
  // console.log(data);
  return request({
    url: requesturl,
    method: 'post',
    data: {
      ...data,
      Header:{
        CmdCode: CmdCode,
      },
      Token: getToken()
    }
  })
}

// 请求海关数据
export function customsrequest(CmdCode, data) {
  return request({
    url: customurl + CmdCode,
    method: 'post',
    data: {
      ...data,
      // Header:{
      //   CmdCode: CmdCode,
      // },
      Token: getToken()
    }
  })
}

// 调用gpt聊天
export function getgptchat(data) {
  return request({
    url: `/ai/chat`,
    method: 'post',
    data: {
      ...data,
      Token: getToken()
    }
  })
}

// 下载
export function downloadfile(CmdCode, data) {
  return request({
    url: downfileurl,
    method: 'post',
    responseType:'arraybuffer',
    data: {
      ...data,
      Header:{
        CmdCode: CmdCode
      },
      Token: getToken()
    }
  })
}

// 下载资源仓库
export function download_token(CmdCode, data) {
  return request({
    url: 'download_token',
    method: 'post',
    responseType:'arraybuffer',
    data: {
      ...data,
      Header:{
        CmdCode: CmdCode
      },
      Token: getToken()
    }
  })
}
// 下载
export function downloadfile_notoken(CmdCode, data) {
  return request({
    url: 'debug',
    method: 'post',
    // responseType:'arraybuffer',
    data: {
      ...data,
      Header:{
        CmdCode: CmdCode
      }
    }
  })
}

// 上传
export function uploadfile(data) {
  return request({
    url: upload,
    method: 'post',
    headers: { 
      'Content-Type': 'application/x-www-form-urlencoded',
      'authorization': 'Bearer ' + getToken(),
    },
    data
  })
}
export function uploadfile2(CmdCode, data) {
  return request({
    url: upload,
    method: 'post',
    data: {
      ...data,
      Header:{
        CmdCode: CmdCode
      },
      Token: getToken()
    }
  })
}

// 升级
export function Upgrade(data) {
  return request({
    url: '/upload',
    method: 'post',
    data
  });
}
// export function downloadfile(CmdCode, data) {
//   return request({
//     url: download,
//     method: 'post',
//     data: {
//       ...data,
//       Header:{
//         CmdCode: CmdCode
//       },
//       Token: getToken()
//     }
//   })
// }



// -- -------------------------------------------------  首页  -------------------------------------------------------------------

// 获取域名审计统计数据
export function GetShadowURLSumGroupByAccessType(data) {
  return request({
    url: requesturl,
    method: 'post',
    data: {
      ...data,
      Header:{
        // CmdCode: 'GetShadowURLSumGroupByAccessType'
        CmdCode: 'GetSchoolSumGroupByStatus'
      },
      Token: getToken()
    }
  })
}

// 今日最近异常域名访问事件
export function GetEventLog(data) {
  return request({
    url: requesturl,
    method: 'post',
    data: {
      ...data,
      Header:{
        CmdCode: 'GetEventLog'
      },
      Token: getToken()
    }
  })
}

// 今日加速流量趋势
export function GetHistorySumStreamLog(data) {
  return request({
    url: requesturl,
    method: 'post',
    data: {
      ...data,
      Header:{
        CmdCode: 'GetHistorySumStreamLog'
      },
      Token: getToken()
    }
  })
}

