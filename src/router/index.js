import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)
const Glod = require('../../public/config')

/* Layout */
import Layout from '@/layout'
import Cookies from 'js-cookie'

/* Router Modules */
// import userPermissionsRouter from './modules/userPermissionsRouter'
import realTimeTrafficRouter from './modules/realTimeTrafficRouter'
import logAnalysisRouter from './modules/logAnalysis'
import journalRouter from './modules/journal'
import securityPerceptionRouter from './modules/securityPerception'
import amazonRouter from './modules/amazonRouter'
import assetRouter from './modules/assetRouter'
import safetyRegulationsRouter from './modules/safetyRegulationsRouter'
import businessConfigurationRouter from './modules/businessConfigurationRouter'
import systemRouter from './modules/systemRouter'
import helpRouter from './modules/helpRouter'
import chatGPTRouter from './modules/chatGPTRouter'
import webRouter from './modules/webRouter'
import MailRouter from './modules/safetyRegulationsRouter'
import resourceRouter from './modules/resourceRouter'
import terminalRouter from './modules/terminalRouter'
import componentsRouter from './modules/components'

/**

注意：当路由的 children.length >= 1 时，子菜单才会显示
详见：https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
hidden: true 如果设置为 true，该项将不会显示在侧边栏中（默认为 false）
alwaysShow: true 如果设置为 true，将始终显示根菜单
如果未设置 alwaysShow，在项目有多个子路由时，它将成为嵌套模式，否则将不显示根菜单
redirect: noRedirect 如果设置为 noRedirect，则面包屑导航不会重定向
name:'router-name' 名称用于 <keep-alive>（必须设置!!!）
meta : { roles: ['admin','editor'] 控制页面角色（可以设置多个角色） title: 'title' 在侧边栏和面包屑导航中显示的名称（推荐设置） 
icon: 'svg-name'/'el-icon-x' 显示在侧边栏中的图标 
noCache: true 如果设置为 true，则页面将不会被缓存（默认为 false） 
affix: true 如果设置为 true，则标签将固定在 tags-view 中 
breadcrumb: false 如果设置为 false，则该项将在面包屑导航中隐藏（默认为 true） 
activeMenu: '/example/list' 如果设置了路径，则侧边栏将突出显示您设置的路径 } */
/**

constantRoutes
不需要权限要求的基本页面
所有角色都可以访问 */

export const constantRoutes = [
  {
    path: '/redirect',
    component: Layout,
    hidden: true,
    children: [
      {
        path: '/redirect/:path(.*)',
        component: () => import('@/views/redirect/index')
      }
    ]
  },
  {
    path: '/login',
    component: () => import('@/views/login/index2'),
    meta: {
      permission: false,
    },
    hidden: true
  },
  {
    path: '/download',
    hidden: true,
    meta: {
      permission: false,
    },
    component: () => import('@/views/download/index')
  },
  // {
  //   path: '/login2',
  //   component: () => import('@/views/login/index2'),
  //   hidden: true
  // },
  {
    path: '/auth-redirect',
    component: () => import('@/views/login/auth-redirect'),
    hidden: true
  },
  {
    path: '/404',
    component: () => import('@/views/error-page/404'),
    hidden: true
  },
  {
    path: '/401',
    component: () => import('@/views/error-page/401'),
    hidden: true
  },
  {
    path: '/',
    component: Layout,
    redirect: '/home',
    permission: '首页',
    disabled: true,
    meta: {
      title: '首页',
      permission: false,
    },
    children: [
      {
        path: 'home',
        component: () => import('@/views/home/index'),
        name: 'home',
        permission: '首页',
        disabled: true,
        meta: { 
          title: '首页',
          icon: 'home', 
          affix: true,
          permission: false,
        }
        // beforeEnter: (to, from, next) => {
        //   // 在每次进入路由前执行逻辑，例如刷新数据
        //   // 使用 next 函数的回调确保组件实例已经被创建
        //   next(vm => {
        //     vm.refreshData();
        //   });
        // }
      }
    ]
  }
]

/**
 * asyncRoutes
 * 根据用户角色需要动态加载的路由
 */
export const asyncRoutes = [
  // userPermissionsRouter,
  realTimeTrafficRouter,
  journalRouter,
  logAnalysisRouter,
  // securityPerceptionRouter,
  amazonRouter,
  businessConfigurationRouter,
  safetyRegulationsRouter,
  assetRouter,
  systemRouter,
  helpRouter,
  // MailRouter,
  // chatGPTRouter,
  // webRouter,
  // resourceRouter,
  // terminalRouter,
  {
    path: '/permission',
    component: Layout,
    hidden: true,
    redirect: '/permission/page',
    alwaysShow: true, // 将始终显示根菜单
    name: 'Permission',
    meta: {
      title: 'permission',
      icon: 'lock',
      roles: ['admin', 'editor'] // 您可以在根导航中设置角色
    },
    children: [
      {
        path: 'page',
        component: () => import('@/views/permission/page'),
        name: 'PagePermission',
        meta: {
          title: 'pagePermission',
          roles: ['admin'] // 或者你只能在子导航中设置角色
        }
      },
      {
        path: 'directive',
        component: () => import('@/views/permission/directive'),
        name: 'DirectivePermission',
        meta: {
          title: 'directivePermission'
          // 如果不设置角色，意味着：该页面不需要权限
        }
      },
      {
        path: 'role',
        component: () => import('@/views/permission/role'),
        name: 'RolePermission',
        meta: {
          title: 'rolePermission',
          roles: ['admin']
        }
      }
    ]
  },

  {
    path: '/icon',
    component: Layout,
    hidden: true,
    permission: false,
    children: [
      {
        path: 'index',
        component: () => import('@/views/icons/index'),
        name: 'Icons',
        meta: { title: 'icons', icon: 'icon', noCache: true, permission: false }
      }
    ]
  },

  /**  当您的路由映射太长时，您可以将其拆分为小模块 **/
  // componentsRouter,
  {
    path: '/error',
    component: Layout,
    hidden: true,
    redirect: 'noRedirect',
    name: 'ErrorPages',
    meta: {
      title: 'errorPages',
      icon: '404',
      permission: false,
    },
    children: [
      {
        path: '401',
        component: () => import('@/views/error-page/401'),
        name: 'Page401',
        meta: { title: 'page401', noCache: true, permission: false,}
      },
      {
        path: '404',
        component: () => import('@/views/error-page/404'),
        name: 'Page404',
        meta: { title: 'page404', noCache: true, permission: false,}
      }
    ]
  },

  {
    path: '/pdf',
    component: Layout,
    redirect: '/pdf/index',
    hidden: true,
    children: [
      {
        path: 'index',
        component: () => import('@/views/pdf/index'),
        name: 'PDF',
        meta: { title: 'pdf', icon: 'pdf' }
      }
    ]
  },
  {
    path: '/pdf/download',
    hidden: true,
    component: () => import('@/views/pdf/download')
  },

  {
    path: '/i18n',
    component: Layout,
    hidden: true,
    children: [
      {
        path: 'index',
        component: () => import('@/views/i18n-demo/index'),
        name: 'I18n',
        meta: { title: 'i18n', icon: 'international' }
      }
    ]
  },

  // 404 页面必须放在最后!!!
  { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

router.beforeResolve((to,from,next)=>{//beforeEach是router的钩子函数，在进入路由前执行
  // const Right = Cookies.get('Public_Right') || []
  const Right = ['全部权限']
  const permission = to.meta.permission // 获取该路由对应的权限码
    // next()
    console.log(Right);
    if(!Right.includes(permission) && Right[0] !== '全部权限' && to.meta.permission !== false){ // 如果用户没有访问该路由的权限，则提示用户无权限访问该页面
      alert('您没有访问该页面的权限！')
      next({
        path: '/home'
      })
    }else { 
      next()
    }
})

export default router
