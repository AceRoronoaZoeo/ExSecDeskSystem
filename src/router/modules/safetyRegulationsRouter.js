/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'
let url = `https://${window.location.hostname}`
const safetyRegulationsRouter = {
  path: '/safetyRegulations',
  component: Layout,
  redirect: '/safetyRegulations/processAudit',
  name: 'safetyRegulations',
    permission: '安全规则',
    meta: {
    title: '安全规则',
    icon: 'lock', 
    permission: '安全规则',
  },
  children: [
    {
      path: 'processAudit',
      name: 'processAudit',
      permission: '进程审计',
      component: () => import('@/views/safetyRegulations/processAudit'),
      meta: { title: '进程审计', icon: 'dian', permission: '进程审计', }
    },
    {
      path: 'networkAudit',
      name: 'networkAudit',
      permission: '网络审计',
      component: () => import('@/views/safetyRegulations/networkAudit'),
      meta: { title: '网络审计', icon: 'dian', permission: '网络审计', }
    },
    {
      path: 'mobileStorage',
      name: 'mobileStorage',
      permission: '移动存储',
      component: () => import('@/views/safetyRegulations/mobileStorage'),
      meta: { title: '移动存储', icon: 'dian', permission: '移动存储', }
    },
    {
      path: 'installSoftware',
      name: 'installSoftware',
      permission: '安装软件',
      component: () => import('@/views/safetyRegulations/installSoftware'),
      meta: { title: '安装软件', icon: 'dian', permission: '安装软件', }
    },
    {
      path: 'externalDeviceAudit',
      name: 'externalDeviceAudit',
      permission: '外设审计',
      component: () => import('@/views/safetyRegulations/externalDeviceAudit'),
      meta: { title: '外设审计', icon: 'dian', permission: '外设审计', }
    },
    // {
    //   path: 'baseline',
    //   name: 'baseline',
    //   permission: '基线核查',
    //   component: () => import('@/views/safetyRegulations/baseline'),
    //   meta: { title: '基线核查', icon: 'dian', permission: '基线核查', }
    // },
    {
      path: 'illegalPort',
      name: 'illegalPort',
      permission: '高危端口',
      component: () => import('@/views/safetyRegulations/illegalPort'),
      meta: { title: '高危端口', icon: 'dian', permission: '高危端口', }
    },
    // {
    //   path: 'processControl',
    //   name: 'processControl',
    //   permission: '进程管控',
    //   component: () => import('@/views/safetyRegulations/processControl'),
    //   meta: { title: '进程管控', icon: 'dian', permission: '进程管控', }
    // },
    // {
    //   path: `${url}:9080/kingsoft.html`,
    //   name: 'King',
    //   permission: '终端安全',
    //   // component: () => import('@/views/safetyRegulations/King'),
    //   meta: { title: '终端安全', icon: 'dian', permission: '终端安全' }
    // },
    // {
    //   path: `${url}:9090/nsfocus.html`,
    //   name: 'green',
    //   permission: '终端接入',
    //   // component: () => import('@/views/safetyRegulations/green'),
    //   meta: { title: '终端接入', icon: 'dian', permission: '终端接入' }
    // },
    // {
    //   path: 'clientGroup',
    //   name: 'clientGroup',
    //   component: () => import('@/views/safetyRegulations/clientGroup'),
    //   meta: { title: '客户分组', icon: 'dian' }
    // },
  ]
}

export default safetyRegulationsRouter
