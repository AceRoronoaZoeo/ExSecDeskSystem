import Layout from '@/layout'
const resourceRouter = {
  path: '/resource',
  component: Layout,
  redirect: '/resource/aiResourceAnalysis',
  name: 'accurateEmail',
  meta: {
    title: '精准资源',
    icon: 'excel',
  },
  children: [
    // {
    //   path: 'emailInformation',
    //   component: () => import('@/views/resource/emailInformation'),
    //   name: 'Icons',
    //   meta: { title: '海关数据', icon: 'dian', noCache: true },
    // },
    {
      path: 'resourceSharing',
      component: () => import('@/views/resource/resourceSharing'),
      name: 'Icons',
      meta: { title: '资源仓库', icon: 'dian', noCache: true, requiresAuth: true, permission: '000001' }
    },
    {
      path: 'aiResourceAnalysis',
      component: () => import('@/views/resource/aiResourceAnalysis'),
      name: 'Icons',
      meta: { title: 'AI资源分析', icon: 'dian', noCache: true }
    },
    // {
    //   path: 'mailResources',
    //   component: () => import('@/views/resource/mailResources'),
    //   name: 'Icons',
    //   meta: { title: '挖掘参数', icon: 'dian', noCache: true },
    // }
  ]
}
export default resourceRouter
