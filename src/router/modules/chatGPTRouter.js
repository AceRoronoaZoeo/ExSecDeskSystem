/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const chatGPTRouter = {
  path: '/chatGPT',
  component: Layout,
  redirect: '/chatGPT/chartGPT',
  name: 'chatGPT',
  alwaysShow: true,
  meta: {
    title: '智能AI',
    icon: 'AI',
  },
  children: [
    {
      path: 'chartGPT',
      name: 'chartGPT',
      component: () => import('@/views/chartGPT/accountManagement'),
      meta: { title: 'chatGPT', icon: 'dian' }
    }
  ]
}

export default chatGPTRouter
