/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const systemRouter = {
  path: '/system',
  component: Layout,
  redirect: '/system/networkConfiguration',
  name: 'system',
  permission: '系统配置',
  meta: {
    title: '系统配置',
    icon: 'shezhiblk',
    permission: '系统配置',
  },
  children: [
    {
      path: 'networkConfiguration',
      name: 'networkConfiguration',
      permission: '网络参数',
      component: () => import('@/views/system/networkConfiguration'),
      meta: { title: '网络参数', icon: 'dian', permission: '网络参数', }
    },
    {
      path: 'systemParameter',
      name: 'systemParameter',
      permission: '系统参数',
      component: () => import('@/views/system/systemParameter'),
      meta: { title: '系统参数', icon: 'dian', permission: '系统参数', }
    },
    {
      path: 'functionalConfiguration',
      name: 'functionalConfiguration',
      permission: '功能配置',
      component: () => import('@/views/system/functionalConfiguration'),
      meta: { title: '功能配置', icon: 'dian', permission: '功能配置', }
    },
    {
      path: 'userPermissions',
      name: 'userPermissions',
      permission: '用户权限',
      component: () => import('@/views/system/userPermissions'),
      meta: { title: '用户权限', icon: 'dian', permission: '用户权限', }
    },
    {
      path: 'systemDictionary',
      name: 'systemDictionary',
      permission: '系统字典',
      component: () => import('@/views/system/systemDictionary'),
      meta: { title: '系统字典', icon: 'dian', permission: '系统字典', }
    },
    // {
    //   path: 'processDictionary',
    //   name: 'processDictionary',
    //   permission: '进程字典',
    //   component: () => import('@/views/system/processDictionary'),
    //   meta: { title: '进程字典', icon: 'dian', permission: '进程字典', }
    // },
    // {
    //   path: 'softwareDictionary',
    //   name: 'softwareDictionary',
    //   permission: '软件字典',
    //   component: () => import('@/views/system/softwareDictionary'),
    //   meta: { title: '软件字典', icon: 'dian', permission: '软件字典', }
    // },
    // {
    //   path: 'eventDictionary',
    //   name: 'eventDictionary',
    //   permission: '事件字典',
    //   component: () => import('@/views/system/eventDictionary'),
    //   meta: { title: '事件字典', icon: 'dian', permission: '事件字典', }
    // },
    // {
    //   path: 'assetsDictionary',
    //   name: 'assetsDictionary',
    //   permission: '资产类型',
    //   component: () => import('@/views/system/assetsDictionary'),
    //   meta: { title: '资产类型', icon: 'dian', permission: '资产类型', }
    // },
    { 
      path: 'equipmentMaintenance',
      name: 'equipmentMaintenance',
      permission: '设备维护',
      component: () => import('@/views/system/equipmentMaintenance'),
      meta: { title: '设备维护', icon: 'dian', permission: '设备维护', }
    },
    { 
      path: 'logProcessing',
      name: 'logProcessing',
      permission: '日志处理',
      component: () => import('@/views/system/logProcessing'),
      meta: { title: '日志处理', icon: 'dian', permission: '日志处理', }
    },
    { 
      path: 'commonTools',
      name: 'commonTools',
      permission: '常用工具',
      component: () => import('@/views/system/commonTools'),
      meta: { title: '常用工具', icon: 'dian', permission: '常用工具', }
    },
    {
      path: 'changePassword',
      name: 'changePassword',
      permission: '修改密码',
      component: () => import('@/views/system/changePassword'),
      meta: { title: '修改密码', icon: 'dian', permission: '修改密码', },
      hidden: true
    }
  ]
}

export default systemRouter
