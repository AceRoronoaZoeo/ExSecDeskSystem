/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const terminalRouter = {
  path: '/terminal',
  component: Layout,
  redirect: '/terminal/terminal',
  name: 'terminal',
  meta: {
    title: '终端管理',
    icon: 'tree',
    
  },
  alwaysShow: true,
  children: [
    {
      path: 'terminal',
      name: 'terminal',
      component: () => import('@/views/terminal/terminal'),
      meta: { title: '终端配置', icon: 'dian' }
    },
    {
      path: 'department',
      name: 'department',
      component: () => import('@/views/asset/department'),
      meta: { title: '部门配置', icon: 'dian' }
    },
  ]
}

export default terminalRouter
