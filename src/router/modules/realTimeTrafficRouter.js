import Layout from '@/layout'
const realTimeTrafficRouter = {
  path: '/realTimeTraffic',
  component: Layout,
  alwaysShow: true,
  redirect: '/realTimeTraffic/terminalState',
  permission: '实时监测',
  meta: {
    title: '实时监测',
    icon: 'zhexian',
  },
  children: [
    // {
    //   path: 'campusStatus',
    //   component: () => import('@/views/realTimeTraffic/index'),
    //   name: 'campusStatus',
    //   permission: '校区状态(实时检测)',
    //   meta: {
    //     title: '校区状态',
    //     icon: 'dian',
    //     permission: '校区状态(实时检测)',
    //     affix: true,
    //   }
    // },
    {
      path: 'terminalState',
      component: () => import('@/views/realTimeTraffic/terminalState'),
      name: 'terminalState',
      meta: { title: '终端状态', icon: 'dian', affix: true },
      permission: '终端状态',
    },
    // {
    //   path: 'assetsStatus',
    //   component: () => import('@/views/realTimeTraffic/assetsStatus'),
    //   name: 'assetsStatus',
    //   permission: '资产状态(实时检测)',
    //   meta: {
    //     title: '资产状态',
    //     icon: 'dian',
    //     permission: '资产状态(实时检测)',
    //     affix: true,
    //   }
    // },
    // {
    //   path: 'shareDirectory',
    //   component: () => import('@/views/realTimeTraffic/shareDirectory'),
    //   name: 'shareDirectory',
    //   permission: '共享目录',
    //   meta: {
    //     title: '共享目录',
    //     icon: 'dian',
    //     permission: '共享目录',
    //     affix: true,
    //   },
    //   hidden: true
    // },
    // {
    //   path: 'asset',
    //   component: () => import('@/views/realTimeTraffic/shareDirectory'),
    //   name: 'asset',
    //   permission: '资产管理(实时监测)',
    //   meta: {
    //     title: '资产管理',
    //     icon: 'dian',
    //     permission: '资产管理(实时监测)',
    //     affix: true,
    //     children:[
    //       {
    //         path: 'processManagement',
    //         name: 'processManagement',
    //         permission: '进程管理',
    //         component: () => import('@/views/asset/processManagement'),
    //         meta: { title: '进程管理', icon: 'dian', permission: '进程管理' }
    //       },
    //       {
    //         path: 'portManagement',
    //         name: 'portManagement',
    //         permission: '端口管理',
    //         component: () => import('@/views/asset/portManagement'),
    //         meta: { title: '端口管理', icon: 'dian', permission: '端口管理' }
    //       },
    //       {
    //         path: 'serviceManagement',
    //         name: 'serviceManagement',
    //         permission: '服务管理',
    //         component: () => import('@/views/asset/serviceManagement'),
    //         meta: { title: '服务管理', icon: 'dian', permission: '服务管理' }
    //       },
    //     ]
    //   }
    // },
    {
      path: 'realTimeScreen',
      name: 'realTimeScreen',
      permission: '实时屏幕',
      component: () => import('@/views/realTimeTraffic/realTimeScreen'),
      meta: {
        title: '实时屏幕',
        icon: 'dian',
        permission: '实时屏幕',
      }
    },
    {
      path: 'remoteControl',
      name: 'remoteControl',
      permission: '远程控制',
      component: () => import('@/views/realTimeTraffic/remoteControl'),
      meta: {
        title: '远程控制',
        icon: 'dian',
        permission: '远程控制',
      }
    },
    // {
    //   path: 'largeScreenPortal',
    //   name: 'largeScreenPortal', 
    //   component: () => import('@/views/realTimeTraffic/largeScreenPortal'),
    //   meta: { title: '大屏门户', icon: 'dian' },
    //   hidden: true
    // },
    {
      path: 'campusDetail',
      name: 'campusDetail',
      permission: '设备详情(实时检测)',
      component: () => import('@/views/realTimeTraffic/campusDetail'),
      meta: { title: '设备详情', icon: 'dian', permission: '设备详情(实时检测)', },
      hidden: true
    },
    {
      path: 'assetDetails',
      name: 'assetDetails',
      permission: '资产详情(实时检测)',
      component: () => import('@/views/realTimeTraffic/assetDetails'),
      meta: { title: '资产详情', icon: 'dian', permission: '资产详情(实时检测)', },
      hidden: true
    },
    {
      path: 'tacticsDetails',
      name: 'tacticsDetails',
      permission: '策略详情(实时检测)',
      component: () => import('@/views/realTimeTraffic/tacticsDetails'),
      meta: { title: '策略详情', icon: 'dian', permission: '策略详情(实时检测)', },
      hidden: true
    },
  ]
}
export default realTimeTrafficRouter
