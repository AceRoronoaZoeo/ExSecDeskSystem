/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const amazonRouter = {
  path: '/amazon',
  component: Layout,
  redirect: '/amazon/accountManagement',
  name: 'amazon',
  permission: '运营助手',
  meta: {
    title: '运营助手',
    icon: 'shopping',
    permission: '运营助手',
    requiresAuth: true,
  },
  children: [
    {
      path: 'accountManagement',
      name: 'accountManagement',
      permission: '远程桌面',
      component: () => import('@/views/amazon/accountManagement'),
      meta: { title: '远程桌面', icon: 'dian', permission: '远程桌面', },
    },
    {
      path: 'sharedServer',
      name: 'sharedServer',
      permission: '文件共享',
      component: () => import('@/views/amazon/sharedServer'),
      meta: { title: '文件共享', icon: 'dian', permission: '文件共享', },
    },
    // {
    //   path: 'accessLog',
    //   name: 'accessLog',
    //   component: () => import('@/views/amazon/accessLog'),
    //   meta: { title: '接入日志', icon: 'dian' },
    // },
  ]
}

export default amazonRouter
