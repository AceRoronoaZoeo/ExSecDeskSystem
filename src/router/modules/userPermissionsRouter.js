import Layout from '@/layout'
const userPermissionsRouter = {
  path: '/userPermissions',
  component: Layout,
  alwaysShow: true,
  redirect: '/userPermissions/department',
  permission: '用户权限',
  meta: {
    title: '用户权限',
    permission: '用户权限',
    icon: 'user',
  },
  children: [
    {
      path: 'userManagement',
      name: 'userManagement',
      permission: '用户管理',
      component: () => import('@/views/userPermissions/userManagement'),
      meta: { title: '用户管理', icon: 'dian' }
    },
    {
      path: 'rolePermissions',
      name: 'rolePermissions',
      permission: '角色权限',
      component: () => import('@/views/userPermissions/rolePermissions'),
      meta: { title: '角色权限', icon: 'dian' }
    }
  ]
}
export default userPermissionsRouter
