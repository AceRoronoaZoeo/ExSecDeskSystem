/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const securityPerceptionRouter = {
  path: '/securityPerception',
  component: Layout,
  redirect: '/securityPerception/safetyAnalysis',
  name: 'securityPerception',
  permission: '安全感知',
  meta: {
    title: '安全感知',
    icon: 'bug',
    permission: '安全感知',
  },
  children:[
    {
      path: 'safetyAnalysis',
      name: 'safetyAnalysis',
      permission: '安全分析',
      component: () => import('@/views/logAnalysis/safetyAnalysis'),
      meta: { 
        title: '安全分析', 
        icon: 'dian',
        permission: '安全分析',
      }
    },
    {
      path: 'baselineVerification',
      name: 'baselineVerification',
      permission: '基线核查(安全感知)',
      component: () => import('@/views/logAnalysis/baselineVerification'),
      meta: { 
        title: '基线核查', 
        icon: 'dian',
        permission: '基线核查(安全感知)',
      }
    },
    {
      path: 'virusDetection',
      name: 'virusDetection',
      permission: '病毒感知',
      component: () => import('@/views/logAnalysis/virusDetection'),
      meta: { 
        title: '病毒感知', 
        icon: 'dian',
        permission: '病毒感知',
      }
    },
    {
      path: 'jinshanTerminal',
      name: 'jinshanTerminal',
      permission: '安全终端',
      component: () => import('@/views/logAnalysis/jinshanTerminal'),
      meta: { 
        title: '安全终端', 
        icon: 'dian',
        permission: '安全终端',
      }
    },
    {
      path: 'networkIsolation',
      name: 'networkIsolation',
      permission: '网络隔离',
      component: () => import('@/views/logAnalysis/networkIsolation'),
      meta: { 
        title: '网络隔离', 
        icon: 'dian',
        permission: '网络隔离',
      }
    },
    {
      path: 'zeroTrustAnalysis',
      name: 'zeroTrustAnalysis',
      permission: '零信任分析',
      component: () => import('@/views/logAnalysis/zeroTrustAnalysis'),
      meta: { 
        title: '零信任分析', 
        icon: 'dian',
        permission: '零信任分析',
      }
    },
  ]
}

export default securityPerceptionRouter
