/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const assetRouter = {
  path: '/asset',
  component: Layout,
  redirect: '/asset/assetOverview',
  name: 'asset',
  permission: '资产管理',
  meta: {
    title: '资产管理',
    icon: 'tree-table',
    requiresAuth: true,
    permission: '资产管理',
  },
  children: [
    // {
    //   path: 'campusManagement',
    //   name: 'campusManagement',
    //   permission: '校区管理',
    //   component: () => import('@/views/asset/campusManagement'),
    //   meta: { title: '校区管理', icon: 'dian', permission: '校区管理' }
    // },
    
    {
      path: 'assetOverview',
      name: 'assetOverview',
      permission: '资产总览',
      component: () => import('@/views/asset/assetOverview'),
      meta: { title: '资产总览', icon: 'dian', permission: '资产总览' }
    },
    {
      path: 'formalAssets',
      name: 'formalAssets',
      permission: '正式资产',
      component: () => import('@/views/asset/formalAssets'),
      meta: { title: '正式资产', icon: 'dian', permission: '正式资产' }
    },
    {
      path: 'assetDetails',
      name: 'assetDetails',
      permission: '资产详情',
      component: () => import('@/views/asset/assetDetails'),
      meta: { title: '资产详情', icon: 'dian', permission: '资产详情' },
      hidden: true,
    },
    {
      path: 'unknownAssets',
      name: 'unknownAssets',
      permission: '未知资产',
      component: () => import('@/views/asset/unknownAssets'),
      meta: { title: '未知资产', icon: 'dian', permission: '未知资产' }
    },
    {
      path: 'assetScanning',
      name: 'assetScanning',
      permission: '资产扫描',
      component: () => import('@/views/asset/assetScanning'),
      meta: { title: '资产扫描', icon: 'dian', permission: '资产扫描' }
    },
    // {
    //   path: 'abnormalAssets',
    //   name: 'abnormalAssets',
    //   permission: '异常资产',
    //   component: () => import('@/views/asset/abnormalAssets'),
    //   meta: { title: '异常资产', icon: 'dian', permission: '异常资产' }
    // },
    // {
    //   path: 'partitionConfiguration',
    //   name: 'partitionConfiguration',
    //   permission: '分区配置',
    //   component: () => import('@/views/asset/partitionConfiguration'),
    //   meta: { title: '分区配置', icon: 'dian', permission: '分区配置' }
    // },
    // {
    //   path: 'department',
    //   name: 'department',
    //   permission: '部门配置',
    //   component: () => import('@/views/asset/department'),
    //   meta: { title: '部门配置', icon: 'dian', permission: '部门配置' }
    // }
  ]
}

export default assetRouter
