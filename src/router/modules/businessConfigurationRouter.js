/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const businessConfigurationRouter = {
  path: '/businessConfiguration',
  component: Layout,
  redirect: '/businessConfiguration/energySavingRules',
  name: 'businessConfiguration',
  permission: '业务配置',
  meta: {
    title: '业务配置',
    icon: 'guide',
    requiresAuth: true,
    permission: '业务配置',
  },
  children: [
    {
      path: 'energySavingRules',
      name: 'energySavingRules',
      permission: '节能规则',
      component: () => import('@/views/businessConfiguration/energySavingRules'),
      meta: { title: '节能规则', icon: 'dian', permission: '节能规则' },
    },
    {
      path: 'corporateIdentity',
      name: 'corporateIdentity',
      permission: '企业形象',
      component: () => import('@/views/businessConfiguration/corporateIdentity'),
      meta: { title: '企业形象', icon: 'dian', permission: '企业形象'}, 
    },
    {    
      path: 'softwareManager',
      name: 'softwareManager',
      permission: '软件管家',
      component: () => import('@/views/businessConfiguration/softwareManager'),
      meta: { title: '软件管家', icon: 'dian', permission: '软件管家' },
    },
    {
      path: 'networkServices',
      name: 'networkServices',
      permission: '网络业务',
      component: () => import('@/views/businessConfiguration/networkServices'),
      meta: { title: '网络业务', icon: 'dian', permission: '网络业务' },
    },
    {
      path: 'softwareDistribution',
      name: 'softwareDistribution',
      permission: '软件分发',
      component: () => import('@/views/businessConfiguration/softwareDistribution'),
      meta: { title: '软件分发', icon: 'dian', permission: '软件分发' },
    },
    {
      path: 'collectionStrategy',
      name: 'collectionStrategy',
      permission: '采集策略',
      component: () => import('@/views/businessConfiguration/collectionStrategy'),
      meta: { title: '采集策略', icon: 'dian', permission: '采集策略' },
    },
    {
      path: 'baseline',
      name: 'baseline',
      permission: '基线核查',
      component: () => import('@/views/safetyRegulations/baseline'),
      meta: { title: '基线核查', icon: 'dian', permission: '基线核查', }
    },
  ]
}

export default businessConfigurationRouter
