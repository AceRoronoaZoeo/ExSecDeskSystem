/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const helpRouter = {
  path: '/help',
  component: Layout,
  redirect: '/help/contactUs',
  name: 'help',
  permission: '关于我们',
  meta: {
    title: '关于我们',
    icon: 'peoples',
    permission: '关于我们'
  },
  alwaysShow: true,
  children: [
    {
      path: 'contactUs',
      name: 'contactUs',
      permission: '关于我们',
      component: () => import('@/views/help/contactUs'),
      meta: { title: '关于我们', icon: 'dian', permission: '关于我们' }
    },
    // {
    //   path: 'systemOverview',
    //   name: 'systemOverview',
    //   component: () => import('@/views/help/help'),
    //   meta: { title: '系统概述', icon: 'dian' }
    // },
    // {
    //   path: 'networkAcceleration',
    //   name: 'networkAcceleration',
    //   component: () => import('@/views/help/help'),
    //   meta: { title: '网络加速', icon: 'dian' }
    // },
    // {
    //   path: 'amazonAssistant',
    //   name: 'amazonAssistant',
    //   component: () => import('@/views/help/help'),
    //   meta: { title: '亚马逊助手', icon: 'dian' }
    // },
    // {
    //   path: 'smartMail',
    //   name: 'smartMail',
    //   component: () => import('@/views/help/help'),
    //   meta: { title: '智能邮件', icon: 'dian' }
    // },
    // {
    //   path: 'artificialIntelligence',
    //   name: 'artificialIntelligence',
    //   component: () => import('@/views/help/help'),
    //   meta: { title: '人工智能', icon: 'dian' }
    // },
    // {
    //   path: 'behavioralAudit',
    //   name: 'behavioralAudit',
    //   component: () => import('@/views/help/help'),
    //   meta: { title: '行为审计', icon: 'dian' }
    // },
    // {
    //   path: 'overseasLiveStreaming',
    //   name: 'overseasLiveStreaming',
    //   component: () => import('@/views/help/help'),
    //   meta: { title: '海外直播', icon: 'dian' }
    // },
    // {
    //   path: 'preciseResources',
    //   name: 'preciseResources',
    //   component: () => import('@/views/help/help'),
    //   meta: { title: '精准资源', icon: 'dian' }
    // },
    // {
    //   path: 'otherFunctions',
    //   name: 'otherFunctions',
    //   component: () => import('@/views/help/help'),
    //   meta: { title: '其他功能', icon: 'dian' }
    // },
  ]
}

export default helpRouter
