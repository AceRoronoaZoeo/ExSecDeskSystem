/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const logAnalysisRouter = {
  path: '/journal',
  component: Layout,
  redirect: '/journal/screenRecording',
  name: 'journal',
  permission: '日志分析',
  meta: {
    title: '日志分析',
    icon: 'chart',
    permission: '日志分析'
  },
  children: [
    {
      path: 'screenRecording',
      name: 'screenRecording',
      permission: '屏幕录像',
      component: () => import('@/views/journal/screenRecording'),
      meta: { 
        title: '屏幕录像', 
        icon: 'dian',
        permission: '屏幕录像',
      }
    },
    {
      path: 'recordingDetails',
      name: 'recordingDetails',
      permission: '录像详情',
      hidden: true,
      component: () => import('@/views/journal/recordingDetails'),
      meta: { 
        title: '录像详情', 
        icon: 'dian',
        permission: '录像详情',
      }
    },
    {
      path: 'chatBackup',
      name: 'chatBackup',
      permission: '沟通回溯',
      component: () => import('@/views/journal/chatBackup'),
      meta: { 
        title: '沟通回溯', 
        icon: 'dian',
        permission: '沟通回溯',
      }
    },
    {
      path: 'workFlow',
      name: 'workFlow',
      permission: '工作流水',
      component: () => import('@/views/journal/workFlow'),
      meta: { 
        title: '工作流水', 
        icon: 'dian',
        permission: '工作流水',
      }
    },
    {
      path: 'jobPerformance',
      name: 'jobPerformance',
      permission: '工作绩效',
      component: () => import('@/views/journal/jobPerformance'),
      meta: { 
        title: '工作绩效', 
        icon: 'dian',
        permission: '工作绩效',
      }
    },
    {
      path: 'domainNameResolution',
      name: 'domainNameResolution',
      permission: '域名解析',
      component: () => import('@/views/journal/domainNameResolution'),
      meta: { 
        title: '域名解析', 
        icon: 'dian',
        permission: '域名解析',
      }
    },
    {
      path: 'operatingSystemLogs',
      name: 'operatingSystemLogs',
      permission: '操作系统日志',
      component: () => import('@/views/journal/operatingSystemLogs'),
      meta: { 
        title: '操作系统日志', 
        icon: 'dian',
        permission: '操作系统日志',
      }
    },
    {
      path: 'assetStatus',
      name: 'assetStatus',
      permission: '资产状态',
      component: () => import('@/views/journal/assetStatus'),
      meta: { 
        title: '资产状态', 
        icon: 'dian',
        permission: '资产状态',
      }
    },
    {
      path: 'systemLog',
      name: 'systemLog',
      permission: '系统日志',
      component: () => import('@/views/journal/systemLog'),
      meta: { 
        title: '系统日志', 
        icon: 'dian',
        permission: '系统日志',
      }
    },
  ]
}

export default logAnalysisRouter
