/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const webRouter = {
  path: '/web',
  component: Layout,
  redirect: '/web/mailTracking',
  name: 'web',
  meta: {
    title: '邮件跟踪',
    icon: 'international',
    requiresAuth: true,
    permission: '010000',
    
  },
  children: [
    {
      path: 'mailTracking',
      name: 'mailTracking',
      component: () => import('@/views/web/mailTracking'),
      meta: { title: '邮件跟踪', icon: 'dian'}
    },
    {
      path: 'nativeContent',
      name: 'nativeContent',
      component: () => import('@/views/web/nativeContent'),
      meta: { title: '邮件撰写', icon: 'dian' }
    },
    {
      path: 'groupResources',
      name: 'groupResources',
      component: () => import('@/views/web/groupResources'),
      meta: { title: '网络图片', icon: 'dian' },
      hidden: true,
    },
    {
      path: 'artificialIntelligence',
      name: 'artificialIntelligence',
      component: () => import('@/views/web/artificialIntelligence'),
      meta: { title: '人工智能', icon: 'dian' },
      hidden: true,
    },
    {
      path: 'mailResources',
      name: 'mailResources',
      component: () => import('@/views/web/mailResources'),
      meta: { title: '群发参数', icon: 'dian' },
      keepAlive: true,
    },
    {
      path: 'massPostingAccount',
      name: 'massPostingAccount',
      component: () => import('@/views/web/massPostingAccount'),
      meta: { title: '群发帐号', icon: 'dian' },
      hidden: true,
    },
  ]
}

export default webRouter
