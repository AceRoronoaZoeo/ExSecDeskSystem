/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const logAnalysisRouter = {
  path: '/logAnalysis',
  component: Layout,
  redirect: '/logAnalysis/eventlog',
  name: 'logAnalysis',
  permission: '安全感知',
  meta: {
    title: '安全感知',
    icon: 'tree',
    permission: '安全感知'
  },
  children: [
    {
      path: 'eventLog',
      name: 'eventLog',
      permission: '事件日志',
      component: () => import('@/views/logAnalysis/eventLog'),
      meta: { 
        title: '事件日志', 
        icon: 'dian',
        permission: '事件日志',
      }
    },
    {
      path: 'baselineVerification',
      name: 'baselineVerification',
      permission: '基线核查(安全感知)',
      component: () => import('@/views/logAnalysis/baselineVerification'),
      meta: { 
        title: '基线核查', 
        icon: 'dian',
        permission: '基线核查(安全感知)',
      }
    },
    {
      path: 'resourceAnalysis',
      name: 'resourceAnalysis',
      permission: '资源分析',
      component: () => import('@/views/logAnalysis/resourceAnalysis'),
      meta: { 
        title: '资源分析', 
        icon: 'dian',
        permission: '资源分析',
      }
    },
    {
      path: 'networkServices',
      name: 'networkServices',
      permission: '网络业务(数据分析)',
      component: () => import('@/views/logAnalysis/networkServices'),
      meta: { 
        title: '网络业务', 
        icon: 'dian',
        permission: '网络业务(数据分析)',
      }
    },
    {
      path: 'softwareExternalRelations',
      name: 'softwareExternalRelations',
      permission: '软件外联',
      component: () => import('@/views/logAnalysis/softwareExternalRelations'),
      meta: { 
        title: '软件外联', 
        icon: 'dian', 
        permission: '软件外联',
        requiresAuth: true, 
      }
    },
    {
      path: 'processLoading',
      name: 'processLoading',
      permission: '进程加载',
      component: () => import('@/views/logAnalysis/processLoading'),
      meta: { 
        title: '进程加载', 
        icon: 'dian', 
        permission: '进程加载',
        requiresAuth: true, 
      }
    },
    {
      path: 'hardwareAssets',
      name: 'hardwareAssets',
      permission: '硬件资产',
      component: () => import('@/views/logAnalysis/hardwareAssets'),
      meta: { 
        title: '硬件资产', 
        icon: 'dian', 
        permission: '硬件资产',
        requiresAuth: true, 
      }
    },
    // {
    //   path: 'softwareUsage',
    //   name: 'softwareUsage',
    //   permission: '软件使用',
    //   component: () => import('@/views/logAnalysis/softwareUsage'),
    //   meta: { title: '软件使用',
    //     icon: 'dian',
    //     permission: '软件使用',
    //     requiresAuth: true,
    //     noCache: false,
    //     keepAlive: true,
    //   }
    // },
    {
      path: 'installingSoftware',
      name: 'installingSoftware',
      permission: '安装软件',
      component: () => import('@/views/logAnalysis/installingSoftware'),
      meta: { 
        title: '安装软件', 
        icon: 'dian', 
        permission: '安装软件',
        requiresAuth: true, 
      }
    },
    // {
    //   path: 'campusStatus',
    //   name: 'campusStatus',
    //   permission: '校区状态',
    //   component: () => import('@/views/logAnalysis/campusStatus'),
    //   meta: { 
    //     title: '校区状态', 
    //     icon: 'dian', 
    //     permission: '校区状态',
    //     requiresAuth: true, 
    //   }
    // },
    // {
    //   path: 'assetStatus',
    //   name: 'assetStatus',
    //   permission: '资产状态',
    //   component: () => import('@/views/logAnalysis/assetStatus'),
    //   meta: { 
    //     title: '资产状态', 
    //     icon: 'dian', 
    //     permission: '资产状态',
    //     requiresAuth: true, 
    //   }
    // },
    
    // {
    //   path: 'systemLog',
    //   name: 'systemLog',
    //   permission: '系统日志',
    //   component: () => import('@/views/logAnalysis/systemLog'),
    //   meta: { 
    //     title: '系统日志', 
    //     icon: 'dian',
    //     permission: '系统日志',
    //   }
    // },
    {
      path: 'selfStartUponStartup',
      name: 'selfStartUponStartup',
      permission: '开机自启',
      component: () => import('@/views/logAnalysis/selfStartUponStartup'),
      meta: { 
        title: '开机自启', 
        icon: 'dian',
        permission: '开机自启',
      }
    },
    {
      path: 'shareDirectory',
      name: 'shareDirectory',
      permission: '共享目录',
      component: () => import('@/views/logAnalysis/shareDirectory'),
      meta: { 
        title: '共享目录', 
        icon: 'dian',
        permission: '共享目录',
      }
    },
    {
      path: 'openPort',
      name: 'openPort',
      permission: '开放端口',
      component: () => import('@/views/logAnalysis/openPort'),
      meta: { 
        title: '开放端口', 
        icon: 'dian',
        permission: '开放端口',
      }
    },
    {
      path: 'virusDetection',
      name: 'virusDetection',
      permission: '病毒检测',
      component: () => import('@/views/logAnalysis/virusDetection'),
      meta: { 
        title: '病毒检测', 
        icon: 'dian',
        permission: '病毒检测',
      }
    },
    {
      path: 'threatAnalysis',
      name: 'threatAnalysis',
      permission: '威胁分析',
      component: () => import('@/views/logAnalysis/threatAnalysis'),
      meta: { 
        title: '威胁分析', 
        icon: 'dian',
        permission: '威胁分析',
      }
    }
  ]
}

export default logAnalysisRouter
