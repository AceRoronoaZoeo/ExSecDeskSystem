const Mock = require('mockjs')

const NameList = []
const count = 100

for (let i = 0; i < count; i++) {
  NameList.push(Mock.mock({
    name: '@first'
  }))
}
NameList.push({ name: 'mock-Pan' })

module.exports = [
  // username search
  {
    url: '/vue-element-admin/search/user',
    type: 'get',
    response: config => {
      const { name } = config.query
      const mockNameList = NameList.filter(item => {
        const lowerCaseName = item.name.toLowerCase()
        return !(name && lowerCaseName.indexOf(name.toLowerCase()) < 0)
      })
      return {
        code: 20000,
        data: { items: mockNameList }
      }
    }
  },

  // transaction list
  {
    url: '/vue-element-admin/transaction/list',
    type: 'get',
    response: _ => {
      return {
        code: 20000,
        data: {
          total: 20,
          'items|20': [{
            'order_no|+1': 1,
            startTime: '@datetime',
            terminal: '@ip',
            address: '@ip',
            url: '@url',
            message: '@cparagraph'
          }]
        }
      }
    }
  },
  {
    url: '/vue-element-admin/GetShadowStreamSum',
    type: 'get',
    response: _ => {
      return {
        code: 20000,
        data: {
          total: 10,
          'items|10': [{
            'order_no|+1': 1,
            ip: '@ip',
            'num|100000-1000000': 100000
          }]
        }
      }
    }
  }
]
